// index.js - activity


// FOR LOOP

let number = prompt("Input initial number:");
console.log("The number you provided is " + number);


for(number = parseInt(number); number >= 0; number--){

	if(number <= 50){
		console.log("The current value is at 50. Terminating the loop.");
		break;
	}

	if(number % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}

	if(number % 5 === 0){
		console.log(number);
		// continue;
	}


}

// MULTIPLICATION TABLE

function multiplication_table(){

	for(let x = 1; x <= 10; x++){
		let product = 5 * x;
		console.log(5 + " x " + x + " = " + product);
	}

}

multiplication_table();