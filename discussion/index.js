// index.js

console.log("Hello World!")

/*

	Mini-Activity
	
	Create a function which will show a message in the console that you want to tell yourself.

	Repeatedly invoke that function. For at least 5 times.

*/


function showMessage(){
	console.log("Take care of yourself.");
}

showMessage();
showMessage();
showMessage();
showMessage();
showMessage();


// WHILE LOOP

//repeats task/code while condition is true

/* 
	while(condition){
		task;
		increment/decrement;
	}
*/

//variable for counter, set how many times to repeat
console.log("============WHILE-LOOP============");

let count_1 = 5;

while(count_1 !== 0){
	console.log(count_1);
	showMessage("Take care of yourself.");
	count_1--;
}

// DO WHILE LOOP

// Do while loop are similar to while loops. However, with a do-while loop, you are able to perform a task at least once even if the condition is not true.


console.log("===========DO-WHILE-LOOP===========");

do {
	console.log(count_1);
	count_1--;
} while(count_1 === 0);
//-this runs once before stopping


// while(count === 0){
// 	console.log(count)
// 	count--;
// }
//-this doesn't run at all

/*

	Mini-Activity

	Create a new variable called counterMini with an initial value of 20.

	Create a while loop which will display a countdown to 1.

*/

console.log("==========MINI-ACTIVITY-1==========");

let counterMini = 20;

while(counterMini !== 0){
	console.log(counterMini);
	counterMini--;
}

// FOR LOOP

/*

The for loop is a more flexible version of our while and do while lops. It consists of 3 parts.
	1. the declaration/initialization of the counter
	2. the condition that will be evaluated to determine if the loop will continue
	3. the iteration or the incrementation/decrementation need to continue and arrive at a termination/end condition
*/
console.log("=============FOR LOOP=============");

for(let count_2 = 0; count_2 <= 20; count_2++){
	console.log(count_2);
}


console.log("==========MINI-ACTIVITY-2==========");

/*

	Mini-Activity

	Debug the ff for loop showing a matrix of a number, an addition table of 1-10.

*/

for(let x = 1; x <= 9; x++){
	let sum = 1 + x;
	console.log("The sum of " + 1 + " + " + x + " = " + sum);
}

// CONTINUE AND BREAK

/*
	Continue is a keyword that allows the code to go to the next loop without finishing the current code block. For the example below, the code block following is disregarded and the next loop is run. Whenever the value of counter is an even number, we skip to the next loop. Therefore, only odd numbers are printed on the console.
*/

for(let count_3 = 0; count_3 <= 20; count_3++){
	if(count_3 % 2 === 0){
		continue;
	}
	console.log(count_3); 

//note: until here the result should be 1,3,5,7,9,11,13,15,17,19


/*
	Break - allows to end the execution of a loop.
*/

	if(count_3 > 10){
		break;
	}
}

//note: but if we add break like this then the result would then be 1,3,5,7,9,11. It will only reach 11 and then ends there. When count_3 === 11 (at the very top) it wouldn't satisfy the first if which is "count_3 % 2 === 0" so it continues to console.log(count_3) and then it proceeds to the second if which is "if(count_3 > 10)". Here, 11 > 10 so it went to "break;". So, it still shows 11 and then cuts it there.



console.log("==========MINI-ACTIVITY-3==========");

/*

	Mini-Activity

	- Using a loop, print all numbers that are divisible by 5.
	- Stop the loop when the loop reaches 100th iteration.

*/

for(let count_4 = 0; count_4 <= 100; count_4++){
	if(count_4 % 5 === 0){
		console.log(count_4);
	} 
